# Zrim-Everything - Eslint Launcher

## Introduction

Docker image to easily launch eslint to a project.

It contains default eslint [configuration](.eslintrc.yml) and [ignore](.eslintignore) file.

## Launcher

The launcher is a simple wrapper to launch eslint executable using 
the default configuration and ignore project. It also allow to specify a different working
directory.

## Usage

```bash
lint -p "pattern" -p "another pattern"
```

This command will launch elint using the given pattern, do not forget to use quote
so the bash will not expand them.

## Advance usage

Options:
- -w/--dir-path <path> : Specify a another working directory
- --verbose : Enable the verbose mode of the application. You can also use the environment
variable `DEBUG=launcher`
- -c/--config-file <path> : Override the default configuration file
- -i/--ignore-file <path> : Override the default ignore file

## Docker

The application is only available via the docker image.
By default the working directory is `/mnt/usr-project`.

Example of a docker file for a project.
```yaml
version: '3.3'
services:
  linter:
    image: registry.gitlab.com/zrim-everything/tools/eslint-launcher:latest
    command: lint -p "/opt/project/lib/**"
    volumes:
      - type: bind
        source: ./
        target: /mnt/usr-project
        read_only: true
networks:
  default:
    external:
      name: none
```

Please consider using a fix version for the image, so you sure to always have the expected version.

You can add multiple path, but be aware that eslint failed if the pattern do not return
any file.

## Link

- [Eslint](https://eslint.org/)
