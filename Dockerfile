FROM node:10.8.0-alpine

LABEL maintainer="NEGRO, Eric <yogo95@zrim-everything.eu>" \
    eu.zrim-everything.project.name="eslint-launcher" \
    eu.zrim-everything.project.type="tool" \
    eu.zrim-everything.project.description="High level application to run eslint" \
    eu.zrim-everything.tools.type="linter"

ENV ZE_LAUNCHER_ROOT_PATH=/usr/local/zrim-everything/eslint-lancher \
    NODE_ENV=production \
    ZE_PROJECT_DIR_PATH=/mnt/usr-project

RUN rm -rf /var/cache/apk/* && \
    rm -rf /tmp/* && \
    apk update && \
    apk upgrade && \
    apk --update --update-cache add --no-cache \
    bash \
    #
    # Clean
    #
    && rm -f /var/cache/apk/* \
    #
    # npm
    #
    && npm install npm@latest -g --unsafe-perm=true \
    #
    # Application directory creation
    #
    && mkdir -p $ZE_LAUNCHER_ROOT_PATH \
    && mkdir -p /etc/eslint/default

COPY .eslintrc.yml .eslintignore /etc/eslint/default/
COPY package.json package-lock.json $ZE_LAUNCHER_ROOT_PATH/
COPY lib/ $ZE_LAUNCHER_ROOT_PATH/lib/

RUN npm set progress=false \
    && npm config set depth 0 \
    && cd $ZE_LAUNCHER_ROOT_PATH \
    && npm install --only=production \
    && npm cache clean --force \
    && chmod a+x $ZE_LAUNCHER_ROOT_PATH/lib/bin/*.js \
    && ln -s $ZE_LAUNCHER_ROOT_PATH/lib/bin/lint.js /usr/bin/lint \
    && ln -s $ZE_LAUNCHER_ROOT_PATH/node_modules/.bin/eslint /usr/bin/eslint

# Set working directory
WORKDIR $ZE_PROJECT_DIR_PATH
CMD ["lint"]

# internal version
ARG VERSION
RUN cd $ZE_LAUNCHER_ROOT_PATH \
    && npm version --allow-same-version --no-git-tag-version $VERSION
