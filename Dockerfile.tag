ARG FROM_REPO
ARG FROM_TAG
FROM $FROM_REPO:$FROM_TAG
ARG VERSION
LABEL eu.zrim-everything.project.version=$VERSION

# update version
RUN cd $ZE_LAUNCHER_ROOT_PATH \
    && npm version --allow-same-version --no-git-tag-version $VERSION
