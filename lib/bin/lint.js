#!/usr/bin/env node

const ArgumentParser = require('argparse').ArgumentParser,
  _ = require('lodash'),
  pathUtils = require('path');

process.exitCode = 1; // Force error if not other exit code specified

const VERSION = require('./../../package.json').version;

const parser = new ArgumentParser({
  version: VERSION,
  addHelp: true,
  description: 'Eslint launcher'
});

parser.addArgument(['-p', '--path'], {
  help: 'Application path the validate',
  dest: 'path',
  action: 'append',
  metavar: 'path',
  required: true
});

parser.addArgument(['-w', '--dir-path'], {
  help: 'Override the working directory for eslint',
  metavar: 'path',
  dest: 'workingDirectory',
  action: 'store',
  defaultValue: process.cwd()
});

parser.addArgument(['--verbose'], {
  help: 'Enable verbose mode',
  dest: 'verboseModeEnabled',
  action: 'storeTrue',
  defaultValue: (process.env.ZE_ENABLE_VERBOSE_MODE || process.env.ENABLE_VERBOSE_MODE) === 'true'
});

parser.addArgument(['-c', '--config-file'], {
  help: 'Override the eslint configuration file',
  metavar: 'path',
  dest: 'eslintConfigFilePath',
  action: 'store',
  defaultValue: '/etc/eslint/default/.eslintrc.yml'
});

parser.addArgument(['-i', '--ignore-file'], {
  help: 'Override the eslint ignore file',
  metavar: 'path',
  dest: 'eslintIgnoreFilePath',
  action: 'store',
  defaultValue: '/etc/eslint/default/.eslintignore'
});


const userArguments = parser.parseArgs();

if (userArguments.verboseModeEnabled === true) {
  process.env.DEBUG = `${process.env.DEBUG},launcher`;
}

const createDebug = require('debug');
const LOGGER = {
  debug: createDebug('launcher')
};


const workflowContext = {
  eslintBin: pathUtils.resolve(__dirname, '../../node_modules/.bin/eslint'),
  locations: [],
  workingDirectory: userArguments.workingDirectory
};

function initializeLintConfigPaths() {
  const rootProjectPath = pathUtils.resolve(__dirname, '../../');
  workflowContext.configFilePath = pathUtils.resolve(rootProjectPath, userArguments.eslintConfigFilePath);
  LOGGER.debug("Using the eslint configuration file '%s'", workflowContext.configFilePath);

  workflowContext.ignoreFilePath = pathUtils.resolve(rootProjectPath, userArguments.eslintIgnoreFilePath);
  LOGGER.debug("Using the eslint ignore file '%s'", workflowContext.ignoreFilePath);
}

/**
 * @typedef {Object} executeGlob~OnResolve
 * @property {String[]} filePaths The file path found
 */
/**
 * Execute glob for a specific pattern
 * @param {string} pattern The pattern
 * @return {Promise<executeGlob~OnResolve>}
 */
function executeGlob(pattern) {
  return new Promise((resolve, reject) => {
    const Glob = require("glob").Glob;

    const globOptions = {
      cwd: workflowContext.workingDirectory,
      mark: true
    };

    LOGGER.debug("Launch glob with pattern '%s'", pattern);
    new Glob(pattern, globOptions, (error, filePaths) => {
      if (error) {
        LOGGER.debug("Glob for pattern '%s' failed:\n%s", pattern, error.message);
        return reject(error);
      }

      filePaths = _.filter(filePaths, el => !_.endsWith(el, '/'));
      LOGGER.debug("Found %d files for the pattern '%s'", filePaths.length, pattern);
      resolve({
        filePaths: filePaths
      });
    });
  });
}

/**
 * @typedef {Object} launchEslintApi~OnResolve
 * @property {Object} report The eslint report
 */
/**
 * Launch eslint using the api
 * @return {Promise<launchEslintApi~OnResolve>}
 */
function launchEslintApi() {
  return new Promise((resolve, reject) => {
    const CLIEngine = require("eslint").CLIEngine;

    const cliEngineOptions = {
      allowInlineConfig: true,
      configFile: workflowContext.configFilePath,
      cwd: workflowContext.workingDirectory,
      ignorePath: workflowContext.ignoreFilePath
    };
    const cliEngine = new CLIEngine(cliEngineOptions);

    Promise.all(_.map(workflowContext.locations, executeGlob))
      .then(results => {
        const filePaths = _.concat.apply(_, _.map(results, 'filePaths'));

        const report = cliEngine.executeOnFiles(filePaths);
        const formatter = cliEngine.getFormatter('stylish');
        process.stdout.write(formatter(report.results) + '\n');

        LOGGER.debug("Result: error=%d warning=%d", report.errorCount, report.warningCount);
        return resolve({
          report: report
        });
      })
      .catch(error => {
        LOGGER.debug("Error: %s\n%s", error.message, error.stack);
        reject(error);
      });
  });
}

/**
 * Initialize the locations
 */
function initLocations() {
  workflowContext.locations = userArguments.path;
}


async function main() {
  initLocations();
  initializeLintConfigPaths();
  const {report} = await launchEslintApi();

  if (report.errorCount === 0) {
    process.exitCode = 0;
  }
}


main()
  .then(() => {
    LOGGER.debug("End of the process");
  })
  .catch(error => {
    LOGGER.debug(`Error: ${error.message}\n${error.stack}\n`);
    process.exit(_.isNumber(error.exitCode) ? error.exitCode : 1);
  });



